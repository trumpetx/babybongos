package com.trumpetx.babybongos.activities;

import roboguice.activity.RoboActivity;
import android.app.KeyguardManager;
import android.app.KeyguardManager.KeyguardLock;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.inject.Inject;
import com.trumpetx.babybongos.app.MySoundManager;
import com.trumpetx.babybongos.app.ShakeEventListener;
import com.trumpetx.babybongos.app.Sound;
import com.trumpetx.babybongos.views.DrawView;

public class BabyBongosActivity extends RoboActivity {

	private DrawView drawView;
	private static final int SECONDS_TO_WAIT = 5;
	protected long exitMillis = 0;
	protected long menuMillis = 0;

	private MySoundManager soundManager;
	@Inject private Vibrator vibrator;
	@Inject	private KeyguardManager keyguardManager;
	@Inject	private SensorManager sensorManager;

	private ShakeEventListener sensorListener;

	private KeyguardLock lock;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		lock = keyguardManager.newKeyguardLock(KEYGUARD_SERVICE);
		
		sensorListener = new ShakeEventListener();
		sensorManager.registerListener(sensorListener,
				sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
				SensorManager.SENSOR_DELAY_UI);

		sensorListener.setOnShakeListener(new ShakeEventListener.OnShakeListener() {
			public void onShake() {
				soundManager.play(Sound.BONGOS);
			}
		});

		this.setVolumeControlStream(AudioManager.STREAM_MUSIC);

		drawView = new DrawView(this);
		drawView.setOnTouchListener(new OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				DrawView view = (DrawView) v;
				if (event.getAction() != MotionEvent.ACTION_DOWN) {
					return v.onTouchEvent(event);
				}

				if (event.getY() > view.getMeasuredHeight() / 2)
					soundManager.play(Sound.DRUM1);
				if (event.getY() < view.getMeasuredHeight() / 2)
					soundManager.play(Sound.DRUM2);
				
				vibrator.vibrate(new long[] { 25, 25 }, -1);

				view.x = event.getX();
				view.y = event.getY();
				view.invalidate();
				return true;
			}
		});

		setContentView(drawView);
		drawView.requestFocus();
	}

	@Override
	public void onAttachedToWindow() {
		this.getWindow().setType(
				WindowManager.LayoutParams.TYPE_KEYGUARD_DIALOG);
		super.onAttachedToWindow();
	}

	private int homeCounter = 0;
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		switch (keyCode) {
			case KeyEvent.KEYCODE_BACK:
			case KeyEvent.KEYCODE_HOME:
				if (exitMillis == 0) {
					exitMillis = System.currentTimeMillis();
				}
				return true;
			case KeyEvent.KEYCODE_MENU:
				return true;
			default:
				//return super.onKeyDown(keyCode, event);
				return false;
		}
	}
	
	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		switch (keyCode) {
			case KeyEvent.KEYCODE_CAMERA:
				//do nothing
				break;
			case KeyEvent.KEYCODE_BACK:
			case KeyEvent.KEYCODE_HOME:
				if (System.currentTimeMillis() - exitMillis > SECONDS_TO_WAIT * 1000) {
					lock.reenableKeyguard();
					this.finish();
				} else {
					if (++homeCounter >=5 ){
						homeCounter = 0;
						Toast.makeText(BabyBongosActivity.this, 
								"To exit app, hold home or back for more than "+SECONDS_TO_WAIT+" seconds and release.",
								Toast.LENGTH_LONG).show();
					}
					exitMillis = 0;
					return true;
				}
			case KeyEvent.KEYCODE_MENU:
				soundManager.loop();
				return true;
		}
		return true;
	}

	@Override
	protected void onResume() {
		super.onResume();
		Log.d("BB", "Resuming BabyBongos");
		sensorManager.registerListener(sensorListener,
				sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
				SensorManager.SENSOR_DELAY_UI);
		soundManager = new MySoundManager(this);
		lock.disableKeyguard();
	}

	@Override
	protected void onStop() {
		Log.d("BB", "Stopping BabyBongos");
		sensorManager.unregisterListener(sensorListener);
		soundManager.destroyMe();
		soundManager = null;
		lock.reenableKeyguard();
		super.onStop();
	}

	@Override
	public void onBackPressed() {
		//nothing
	}

	@Override
	public boolean onKeyLongPress(int keyCode, KeyEvent event) {
		return true;
	}

	@Override
	public boolean onKeyMultiple(int keyCode, int repeatCount, KeyEvent event) {
		return true;
	}
	
	
}