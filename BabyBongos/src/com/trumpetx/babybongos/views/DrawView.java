package com.trumpetx.babybongos.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;

import com.trumpetx.babybongos.R;

public class DrawView extends View {

    public float x=0;
    public float y=0;
    
    Paint paint = new Paint();
    Bitmap bongo = BitmapFactory.decodeResource(getResources(), R.drawable.bongo_topdown);
    

    
    public DrawView(Context context) {
        super(context);
        setFocusable(true);
        setFocusableInTouchMode(true);

        this.setBackgroundResource(R.drawable.beach);
        
        
        paint.setColor(Color.YELLOW);
        paint.setAntiAlias(true);
    }

    @Override
    public void onDraw(Canvas canvas) {
    	int widthPlacement = canvas.getWidth() / 2 - bongo.getWidth() / 2;
    	int heightPlacement = (canvas.getHeight() - bongo.getHeight()) / 2;
    	
    	canvas.drawBitmap(bongo, widthPlacement, heightPlacement, paint);
        if (x != 0){
        	canvas.drawCircle(x, y, 25, paint);
        }
    }
    
}

