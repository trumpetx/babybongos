package com.trumpetx.babybongos.app;

import com.google.inject.AbstractModule;


public class BabyBongosModule extends AbstractModule {

	public BabyBongosModule() {
		super();
	}

	@Override
	protected void configure() {
		requestStaticInjection(MySoundManager.class);

	}

}
