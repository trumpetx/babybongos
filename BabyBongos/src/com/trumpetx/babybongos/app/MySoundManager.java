package com.trumpetx.babybongos.app;

import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;
import android.media.SoundPool.OnLoadCompleteListener;

import com.trumpetx.babybongos.R;

public class MySoundManager {
	
	private Context context;
	private AudioManager audioManager;
	
	private SoundPool soundPool;
	private SoundPool loopSoundPool;

	private Map<Sound, Integer> sounds = new HashMap<Sound, Integer>(3);
	
	private SoundPool loopSoundPoolBuilder(){
		SoundPool answer = new SoundPool(1, AudioManager.STREAM_MUSIC, 100);
		answer.setOnLoadCompleteListener(new OnLoadCompleteListener() {
			
			public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
					soundPool.play(sampleId, getVol() * 0.5f, getVol() * 0.5f, 2, -1, 1f);
			}
			
		});
		return answer;
	}
	
	public MySoundManager(Context context) {
		this.context = context;
		
		audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
		soundPool = new SoundPool(3, AudioManager.STREAM_MUSIC, 100);
		sounds.put(Sound.BONGOS, soundPool.load(context, R.raw.bongos, 1));
		sounds.put(Sound.DRUM1, soundPool.load(context, R.raw.drum1, 1));
		sounds.put(Sound.DRUM2, soundPool.load(context, R.raw.drum2, 1));

	}

	public void play(Sound sound){
		float lVol = getVol();
		float rVol = getVol();
		if (sound == Sound.DRUM1){
			lVol = .5f * lVol;
			rVol = .25f * rVol;
		}
		if (sound == Sound.DRUM2){
			lVol = .25f * lVol;
			rVol = .5f * rVol;
		}
		soundPool.play(sounds.get(sound), lVol, rVol, 1, 0, 1f);
	}
	
	private Integer[] loops = new Integer[] {R.raw.drumloop1, R.raw.drumloop2, null};
	private int loopIndex = -1;
	public void loop(){
		if (++loopIndex >= loops.length)
			loopIndex = 0;
		if (loopSoundPool != null)
			loopSoundPool.release();
		loopSoundPool = null;
		
		Integer resource = loops[loopIndex];
		if (resource == null){			
			return;
		}
		loopSoundPool = loopSoundPoolBuilder();
		loopSoundPool.load(context,loops[loopIndex], 1);
		
	}

	private final float getVol(){
		float actualVolume = (float) audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
		float maxVolume = (float) audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
		return actualVolume / maxVolume;
	}
	
	public void destroyMe(){
		if (soundPool != null){
			soundPool.release();
			soundPool = null;
		}
		if (loopSoundPool != null){
			loopSoundPool.release();
			loopSoundPool = null;
		}
	}

}
